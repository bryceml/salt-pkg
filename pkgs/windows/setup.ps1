# Hide the progress bar
$ProgressPreference = "SilentlyContinue"

# Enable TLS 1.2
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]'Tls,Tls11,Tls12'

# Install Python3
Invoke-WebRequest -Uri "https://repo.saltproject.io/windows/dependencies/64/python-3.8.8-amd64.exe" -OutFile "$env:TEMP\python38.exe"
Start-Process "$env:TEMP\python38.exe" -ArgumentList "/Quiet InstallAllUsers=1 TargetDir=`"C:\Python38`" Include_doc=0 Include_tcltk=0 Include_test=0 Include_launcher=1 PrependPath=1 Shortcuts=0" -Wait -NoNewWindow -PassThru
Remove-Item "$env:TEMP\python38.exe"

# Set Path
$env:PATH += ";C:\Python38;C:\Python38\Scripts"

# Copy DLLs
Invoke-WebRequest -Uri "https://repo.saltproject.io/windows/dependencies/64/libsodium/1.0.18/libsodium.dll" -OutFile "C:\Python38\libsodium.dll"
Invoke-WebRequest -Uri "https://repo.saltproject.io/windows/dependencies/64/openssl/1.1.1k/libeay32.dll" -OutFile "C:\Python38\libeay32.dll"
Invoke-WebRequest -Uri "https://repo.saltproject.io/windows/dependencies/64/openssl/1.1.1k/ssleay32.dll" -OutFile "C:\Python38\ssleay32.dll"
Invoke-WebRequest -Uri "https://repo.saltproject.io/windows/dependencies/64/ssm-2.24-103-gdee49fc.exe" -OutFile "C:\Python38\ssm.exe"
