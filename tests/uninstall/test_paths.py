"""
Test to ensure paths were handled correctly during uninstall
"""
import glob
import os
import pathlib
import sys

import pytest


def test_opt_saltstack():
    """
    test the paths in /opt/saltstack/ were correctly
    removed or not removed
    """

    if sys.platform.startswith("win"):
        # I'm not sure where the /opt/saltstack path is coming from
        # This is the path we're using to test windows
        opt_path = pathlib.Path(os.getenv("LocalAppData")) / "salt" / "pypath"
    else:
        opt_path = pathlib.Path(f"{os.sep}opt", "saltstack", "salt", "pypath")
    if not opt_path.exists():
        if sys.platform.startswith("win"):
            assert not opt_path.parent.exists()
        else:
            assert not opt_path.parent.parent.exists()
    else:
        paths = glob.glob(str(opt_path) + "/**/*", recursive=True)
        for path in paths:
            if pathlib.Path(path).is_file():
                pytest.fail(f"There is a file in the path {opt_path}")
