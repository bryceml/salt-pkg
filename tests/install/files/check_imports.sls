#!py
import importlib

def run():
    config = {}
    try:
        for test_import in ['templates', 'platform', 'cli', 'executors', 'config', 'wheel', 'netapi', 'cache', 'proxy', 'transport', 'metaproxy', 'modules', 'tokens', 'matchers', 'acl', 'auth', 'log', 'engines', 'client', 'returners', 'runners', 'tops', 'output', 'daemons', 'thorium', 'renderers', 'states', 'cloud', 'roster', 'beacons', 'pillar', 'spm', 'utils', 'sdb', 'fileserver', 'defaults', 'ext', 'queues', 'grains', 'serializers']:
            importlib.import_module("salt.{}".format(test_import))
        config['test_imports_succeeded'] = {
            'test.succeed_without_changes': [
                {'name': "test_imports"},
              ],
        }
    except ModuleNotFoundError as err:
        config['test_imports_failed'] = {
            'test.fail_without_changes': [
                {'name': "The imports test failed. The error was: {}".format(err)},
                    ],
            }
    return config
