import sys

import tests.support.helpers


def test_help(salt_fixt):
    """
    Test --help works for all salt cmds
    """
    for cmd in salt_fixt.salt_bin.values():
        ret = tests.support.helpers.run(cmd + ["--help"])
        # We don't use pip on Windows... yet. Waiting for changes to tiamat-pip
        if "pip" in cmd and sys.platform.startswith("win"):
            continue
        assert "Usage" in ret["stdout"]
        assert ret["retcode"] == 0
