def test_salt_pillar(salt_pillar, master_minion, salt_fixt):
    """
    Test pillar.items
    """
    ret = salt_fixt.salt_minion(["pillar.items"])
    assert "info" in ret["stdout"]
