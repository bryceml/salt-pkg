from sys import platform

import pytest


def test_check_imports(master_minion, salt_fixt):
    """
    Test imports
    """
    ret = salt_fixt.salt_minion(["state.sls", "check_imports"])
    assert "test_imports_succeeded" in str(ret)
    assert "test_imports_failed" not in str(ret)
