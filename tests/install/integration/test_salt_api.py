from tests.support.helpers import api


def test_salt_api(salt_api):
    """
    Test running a command against the salt api
    """
    ret = api(
        {
            "client": "local",
            "tgt": "*",
            "fun": "test.arg",
            "arg": ["foo", "bar"],
        }
    )
    assert ret["args"] == ["foo", "bar"]
