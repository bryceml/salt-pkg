import subprocess
from sys import platform

import pytest


@pytest.mark.skipif(platform.startswith("win"), reason="Linux test only")
def test_system_config(master_minion, salt_fixt):
    """
    Test system config
    """
    get_family = salt_fixt.salt_minion(["grains.get", "os_family"])
    get_finger = salt_fixt.salt_minion(["grains.get", "osfinger"])

    if "RedHat" in get_family["stdout"]:
        if "CentOS Linux-8" in get_finger["stdout"]:
            ret = subprocess.call(
                "systemctl show -p ${config} salt-minion.service", shell=True
            )
            assert ret == 0
        else:
            ret = subprocess.call(
                "systemctl show -p ${config} salt-minion.service", shell=True
            )
            assert ret == 1

    elif "Debian" in get_family["stdout"]:
        if "Debian-9" in get_finger["stdout"]:
            ret = subprocess.call(
                "systemctl show -p ${config} salt-minion.service", shell=True
            )
            assert ret == 1
        else:
            ret = subprocess.call(
                "systemctl show -p ${config} salt-minion.service", shell=True
            )
            assert ret == 0
