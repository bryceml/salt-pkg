import os
import sys

import tests.support.helpers


def test_salt_version(version, salt_fixt):
    """
    Test version outputed from salt --version
    """
    ret = tests.support.helpers.run(salt_fixt.salt_bin["salt"] + ["--version"])

    # On Windows the name of the binary is returned. For onedir, that is
    # `run.exe`. For singlebin, `salt.exe`. One linux, it is always `salt`
    if sys.platform.startswith("win"):
        bin_name = os.path.basename(salt_fixt.salt_bin["salt"][0])
    else:
        bin_name = "salt"

    assert ret["stdout"].strip() == f"{bin_name} {version}"


def test_salt_versions_report_master(salt_fixt):
    """
    Test running --versions-report on master
    """
    ret = salt_fixt.salt_master(["--versions-report"])
    assert "Salt Version:" in ret["stdout"]


def test_salt_versions_report_minion(master_minion, salt_fixt):
    """
    Test running test.versions_report on minion
    """
    ret = salt_fixt.salt_minion(["test.versions_report"])
    assert "Salt Version:" in ret["stdout"]
