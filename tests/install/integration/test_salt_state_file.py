import sys


def test_salt_state_file(sls, master_minion, salt_fixt):
    """
    Test state file
    """
    if sys.platform.startswith("win"):
        ret = salt_fixt.salt_minion(["state.apply", "win_states"])
    else:
        ret = salt_fixt.salt_minion(["state.apply", "states"])

    sls_ret = ret["stdout"][next(iter(ret["stdout"]))]
    assert "changes" and "name" in sls_ret
