def test_salt_call_local(master_minion, salt_fixt):
    """
    Test salt-call --local test.ping
    """
    ret = salt_fixt.salt_call_local(["test.ping"])
    assert ret["stdout"] is True
    assert ret["retcode"] == 0


def test_salt_call(master_minion, salt_fixt):
    """
    Test salt-call test.ping
    """
    ret = salt_fixt.salt_call(["test.ping"])
    assert ret["stdout"]
    assert ret["retcode"] == 0


def test_sls(sls, master_minion, salt_fixt):
    """
    Test calling a sls file
    """
    ret = salt_fixt.salt_call(["state.apply", "test"])
    sls_ret = ret["stdout"][next(iter(ret["stdout"]))]
    assert sls_ret["changes"]["testing"]["new"] == "Something pretended to change"
    assert ret["retcode"] == 0


def test_salt_call_local_sys_doc_none(master_minion, salt_fixt):
    """
    Test salt-call --local sys.doc none
    """
    ret = salt_fixt.salt_call_local(["sys.doc", "none"])
    assert not ret["stdout"]
    assert ret["retcode"] == 0


def test_salt_call_local_sys_doc_aliasses(master_minion, salt_fixt):
    """
    Test salt-call --local sys.doc aliasses
    """
    ret = salt_fixt.salt_call_local(["sys.doc", "aliases.list_aliases"])
    assert "aliases.list_aliases" in ret["stdout"]
    assert ret["retcode"] == 0
